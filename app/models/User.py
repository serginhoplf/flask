from app import db
from flask_restful import fields, marshal_with, Resource

resource_fields = {
    'username':   	fields.String,
    'name':			fields.String,
    'email':		fields.String
}

class User(db.Model):

	v = 'n'

	__tablename__ = 'users'

	id = db.Column(db.Integer, autoincrement=True, primary_key=True)
	username = db.Column(db.String, nullable=False)
	password = db.Column(db.String, nullable=False)
	name = db.Column(db.String, nullable=False)
	email = db.Column(db.String, nullable=False)

	def __init__(self, username, password, name, email):
		self.username = username
		self.password = password
		self.name = name
		self.email = email

	
	def get_all():
		return User.query.all()

	def get_all_json():
		usuarios = []

		for user in User.get_all():
			usuarios.append({'username': user.username, 'password': user.password})

		return usuarios

class UserResource(Resource):
	
	@marshal_with(resource_fields)
	def get_all():
		return User.get_all()
