from app import db
import json
"""
class User(db.Model):

	__tablename__ = 'users'

	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	username = db.Column(db.String, unique=True, nullable=False)
	password = db.Column(db.String(8), nullable=False)
	name = db.Column(db.String, nullable=False)
	email = db.Column(db.String, unique=True, nullable=False)

	def __init__(self, username, password, name, email):
		self.username = username
		self.password = password
		self.name = name
		self.email = email


	def get_all():
		return User.__get_all_json()

	def __get_all_json():
		usuarios = []
		for user in User.query.all():
			usuarios.append({'username':user.username, 'password': user.password})
		return usuarios
"""
class Post(db.Model):

	__tablename__ = 'posts'

	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	content = db.Column(db.Text, nullable=False)
	user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
	
	user = db.relationship('User', 
		foreign_keys=user_id)

	def __init__(self, content, user_id):
		self.content = content
		self.user_id = user_id

	#def __repr__(self):
		#return "<Post: %r>" %user.name

class Follow(db.Model):

	__tablename__ = 'follow'

	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
	follower_id = db.Column(db.Integer, db.ForeignKey('users.id'))

	user = db.relationship('User', 
		foreign_keys=user_id)

	follower = db.relationship('User', 
		foreign_keys=follower_id)

	def __init__(self, user_id, follower_id):
		self.user_id = user_id
		self.follower_id = follower_id