from app import db, app

from app.models.User import User, UserResource
from flask import jsonify, Response

@app.route('/cadastrar/<username>/<password>/<name>/<email>')
def cadastrar(username, password, name, email):
	user = User(username, password, name, email)
	db.session.add(user)
	db.session.commit()
	return jsonify(username=username)

@app.route('/listar')
def listar():
	return jsonify(UserResource.get_all())

